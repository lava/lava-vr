#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable


layout (location = 0) in vec2 vTexCoord;
layout (location = 1) flat in uint vLayer;

layout (location = 0) out vec4 fColor;

layout (set = 1, binding = 0) uniform sampler2DArray uTexture;

void main() {
    fColor = texture(uTexture, vec3(vTexCoord,vLayer));
    if (fColor.a < 0.5) discard;
    //if (pu.premultiplied && false) fColor.rgb /= fColor.a;
}
