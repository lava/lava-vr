#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_EXT_multiview : enable
#extension GL_NVX_multiview_per_view_attributes : enable


layout(location=0) in vec3 aPosition;
layout(location=1) in vec2 aTexCoord;
layout(location=2) in uint aLayer;

layout (location = 0) out vec2 vTexCoord;
layout (location = 1) flat out uint vLayer;

layout(push_constant) uniform PushConstants {
    mat4 modelMatrix;
};


layout(set = 0, binding = 0) uniform CameraMatrices {
    mat4 view[2];
    mat4 proj[2];
} cams;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main() {
    vTexCoord = aTexCoord;
    vLayer = aLayer;

    //vec4 worldPos = modelMatrix * vec4(aPosition, 1.0);
    vec4 worldPos = modelMatrix * vec4(aPosition, 1.0);

    gl_PositionPerViewNV[0] =
        cams.proj[0] * cams.view[0] * worldPos;
    gl_PositionPerViewNV[1] =
        cams.proj[1] * cams.view[1] * worldPos;
    gl_Position = cams.proj[gl_ViewIndex]
                * cams.view[gl_ViewIndex]
                * worldPos;
}
