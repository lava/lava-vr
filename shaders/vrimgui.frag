#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable


layout (location = 0) in vec2 vTexCoord;

layout(push_constant) uniform PushConstants {
    mat4 modelMatrix;
    float opacity;
};

layout (set = 1, binding = 0) uniform sampler2D uTexture;

layout (location = 0) out vec4 fColor;

void main() {
    fColor = texture(uTexture, vTexCoord);
    fColor.a *= opacity;
    //fColor = vec4(1.0);
    //if (fColor.a < 0.01) discard;
    //if (pu.premultiplied && false) fColor.rgb /= fColor.a;
}
