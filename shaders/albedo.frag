#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable


layout (location = 0) in vec3 vNormal;
layout (location = 1) in vec2 vTexCoord;

layout (location = 0) out vec4 fColor;

layout (set = 1, binding = 0) uniform sampler2D uTexture;

void main() {
    fColor = texture(uTexture, vTexCoord * vec2(1, -1)) * (0.95 * normalize(vNormal).y + 0.05);
}
