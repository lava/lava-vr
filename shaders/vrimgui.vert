#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_EXT_multiview : enable
#extension GL_NVX_multiview_per_view_attributes : enable

layout (location = 0) out vec2 vTexCoord;

layout(push_constant) uniform PushConstants {
    mat4 modelMatrix;
    float opacity;
};

layout(set = 0, binding = 0) uniform CameraMatrices {
    mat4 view[2];
    mat4 proj[2];
} cams;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main() {
    //vTexCoord = vec2(gl_VertexIndex & 2, gl_VertexIndex & 1);
    vTexCoord = vec2(gl_VertexIndex & 1, (gl_VertexIndex >> 1) & 1);

    vec4 pos = vec4(vTexCoord.x - 0.5, 0.0, vTexCoord.y - 1.0, 1.0);
    vec4 worldPos = modelMatrix * pos;

    gl_PositionPerViewNV[0] =
        cams.proj[0] * cams.view[0] * worldPos;
    gl_PositionPerViewNV[1] =
        cams.proj[1] * cams.view[1] * worldPos;
    gl_Position = cams.proj[gl_ViewIndex]
                * cams.view[gl_ViewIndex]
                * worldPos;
}
