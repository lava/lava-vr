# lava-vr

Higher-level utility classes and functions for VR-Applications based on lava and lava-extras.

# Features

## RopeLocomotion

Use two controllers to move around in the scene in a multi-touch / "Mime pulling an invisible rope" fashion.

## BatchingRenderer

Manages an arbitrary amount of meshes with texture and records drawing requests that can be executed at once with a single bind of pipeline and buffers.

Adding a model and texture to the renderer returns a `MeshHandle` which can be used to enque draw requests.

This is mainly meant to be used by `Placers`, classes that help with rendering things, but don't manage any resources of their own.

### FeetPlacer

A simple Placer that takes a MeshHandle of a right foot and pointers to OpenVRController instances and render them as the feet of the player (the mesh will be mirrored for the left foot automatically).

## VRMesh / VRKDTree

Container for a large mesh that covers all / a large portion of the whole scene. Can load and write binary .vrmesh files. Comes with a KDTree implementation that can be used for frustum culling.

## VRMeshRenderer

Renders a VRMesh (TODO: frustum culling).

## FeetElevation

Tracks the user's feet to allow for vertical interaction with a VRMesh scene
