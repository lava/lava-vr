#pragma once
#include <array>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <lava-extras/openvr/fwd.hh>

namespace lava {
namespace vr {

class RopeLocomotion {
  public:
    struct Config {
        bool allowZoom = true;
        bool allowRotation = true;

        bool disableHand[2] = {false, false};
    };

    RopeLocomotion(lava::openvr::SharedOpenVRHmd const &output);
    glm::mat4 updateMatrix(glm::mat4 old);

    bool active() const { return mPressed[0] || mPressed[1]; }

    Config& config() { return mConfig; }

  protected:
    lava::openvr::SharedOpenVRHmd mOpenVR;
    std::array<bool, 2> mPressed;
    std::array<glm::vec3, 2> mLastPos;
    Config mConfig;
};

} // namespace vr
} // namespace lava
