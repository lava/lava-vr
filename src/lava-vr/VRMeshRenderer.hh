#pragma once
#include "VRMesh.hh"
#include <glm/mat4x4.hpp>
#include <lava/fwd.hh>

namespace lava {
namespace vr {

class VRMeshRenderer {
  public:
    VRMeshRenderer(lava::Subpass const &forwardPass,
                   lava::SharedDescriptorSetLayout const &cameraLayout);
    void loadFile(std::string const &filename);
    void upload(VRMesh const &mesh);

    void draw(lava::InlineSubpass &sub,
              lava::SharedDescriptorSet const &cameraSet);

    void setTransform(glm::mat4 const &t) { mModelMatrix = t; }
    glm::mat4 const &getTransform() const { return mModelMatrix; }

  protected:
    glm::mat4 mModelMatrix;

    lava::SharedGraphicsPipeline mPipeline;
    lava::SharedPipelineLayout mLayout;

    lava::SharedBuffer mVertexBuffer;
    lava::SharedBuffer mIndexBuffer;
    lava::SharedImage mTexture;

    lava::SharedDescriptorSetLayout mTextureLayout;
    lava::SharedDescriptorSet mTextureSet;
    lava::SharedSampler mSampler;
};
} // namespace vr
} // namespace lava
