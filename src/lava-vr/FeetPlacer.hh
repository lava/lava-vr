#pragma once
#include <lava-extras/openvr/fwd.hh>
#include <lava-vr/BatchingRenderer.hh>

namespace lava {
namespace vr {

class FeetPlacer {
  public:
    using MeshHandle = lava::vr::BatchingRenderer::MeshHandle;
    using Tracker = lava::openvr::OpenVRController;

    FeetPlacer(MeshHandle foot, Tracker const &left, Tracker const &right);
    void place(lava::vr::BatchingRenderer &renderer);

  protected:
    MeshHandle mFoot;
    Tracker const &mLeft;
    Tracker const &mRight;
};

} // namespace vr
} // namespace lava
