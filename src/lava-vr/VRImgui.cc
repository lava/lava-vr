#include "VRImgui.hh"
#include <lava-extras/imgui/ImGui.hh>
#include <lava-extras/openvr/OpenVRControllerInput.hh>
#include <lava-extras/pack/pack.hh>

#include <lava/createinfos/Buffers.hh>
#include <lava/createinfos/GraphicsPipelineCreateInfo.hh>
#include <lava/createinfos/Images.hh>
#include <lava/createinfos/Sampler.hh>

#include <lava/objects/Buffer.hh>
#include <lava/objects/DescriptorSet.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/GraphicsPipeline.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/RenderPass.hh>

#include <lava/raii/ActiveRenderPass.hh>

#include <glm/gtx/transform.hpp>
#include <glm/matrix.hpp>

namespace lava {
namespace vr {

namespace {
constexpr auto pixels_per_meter = 500.0f;
struct PushConsts {
    glm::mat4 modelMatrix;
    float opacity;
};
}

VRImgui::VRImgui(const Subpass &forwardPass,
                 const SharedDescriptorSetLayout &cameraLayout, float width,
                 float height)
    : mWidth(width), mHeight(height) {
    auto const &device = forwardPass.pass->device();

    auto image =
        lava::attachment2D(uint32_t(width * pixels_per_meter),
                           uint32_t(height * pixels_per_meter), Format::RGBA8)
            .create(device);
    image->realizeVRAM();
    auto view = image->createView();

    mImgui = std::make_unique<imgui::ImGui>(
        device, vk::ImageLayout::eShaderReadOnlyOptimal);
    mImgui->connectViews({view});

    auto texLayout =
        lava::DescriptorSetLayoutCreateInfo().addCombinedImageSampler().create(
            device);

    auto layout =
        device->createPipelineLayout<PushConsts>({cameraLayout, texLayout});

    auto ci = lava::GraphicsPipelineCreateInfo::defaults();
    ci.setLayout(layout);

    ci.depthStencilState.setDepthTestEnable(true).setDepthWriteEnable(true);
    ci.rasterizationState.setFrontFace(vk::FrontFace::eClockwise);
    ci.inputAssemblyState.setTopology(vk::PrimitiveTopology::eTriangleStrip);
    ci.colorBlendState.clear().addTransparencyBlend();

    ci.addStage(lava::pack::shader(device, "lava-vr/vrimgui.vert"));
    ci.addStage(lava::pack::shader(device, "lava-vr/vrimgui.frag"));

    mPipeline = forwardPass.createPipeline(ci);

    auto sampler = device->createSampler({});

    mDescriptor = texLayout->createDescriptorSet();
    mDescriptor->write().combinedImageSampler(sampler, view);
}

VRImGuiFrame VRImgui::frame() {
    auto frm = mImgui->frame();
    frm.active = false;

    ::ImGuiWindowFlags flags = ::ImGuiWindowFlags_NoTitleBar |        //
                               ::ImGuiWindowFlags_NoResize |          //
                               ::ImGuiWindowFlags_NoMove |            //
                               ::ImGuiWindowFlags_NoScrollbar |       //
                               ::ImGuiWindowFlags_NoScrollWithMouse | //
                               ::ImGuiWindowFlags_NoCollapse;
    ::ImGui::Begin("", nullptr, flags);
    ::ImGui::SetWindowPos({0, 0});
    ::ImGui::SetWindowSize(
        {mWidth * pixels_per_meter, mHeight * pixels_per_meter});
    return {};
}

VRImGuiFrame::~VRImGuiFrame() {
    if (active) {
        ::ImGui::End();
        ::ImGui::Render();
    }
}

void VRImgui::update(const glm::mat4 &screenPose,
                     const glm::vec3 &finger,
                     RecordingCommandBuffer &cmd) {
    mPose = screenPose * scale(glm::vec3{mWidth, 1.0f, mHeight});
    auto invPose = inverse(mPose);

    auto pointer = invPose * glm::vec4(finger, 1.0f);

    auto vmouse = glm::vec3(pointer.x + 0.5, pointer.y, 1.0 + pointer.z);
    vmouse.x *= mWidth * pixels_per_meter;
    vmouse.z *= mHeight * pixels_per_meter;
    mVMouse = vmouse;

    mImgui->makeCurrent();
    auto& io = ::ImGui::GetIO();

    if (pointer.y > -0.1f && pointer.y < 0.1f) {
        io.MousePos = {vmouse.x, vmouse.z};
    } else {
        // prevent hover-effects from sticking
        io.MousePos = {-9000, -9000};
    }

    io.MouseDown[0] = (enabled() && pointer.y > -0.1f && pointer.y < 0.01f);

    auto dist = mTargetOpacity - mCurrentOpacity;
    mCurrentOpacity += std::copysign(std::min(std::abs(dist), 0.03f), dist);

    if (visible()) {
        mImgui->render(0, cmd);
    }
}

void VRImgui::draw(InlineSubpass &cmd, SharedDescriptorSet const &cameraSet) {
    if (visible()) {
        cmd.bindPipeline(mPipeline);
        cmd.bindDescriptorSets({cameraSet, mDescriptor});

        cmd.pushConstantBlock<PushConsts>({mPose, mCurrentOpacity});
        cmd.draw(4);
    }
}

void VRImgui::toggle()
{
    mTargetOpacity = 1.0f * (mTargetOpacity < 0.5f);
}

bool VRImgui::visible() const
{
    return mCurrentOpacity > (1.f / 255.f);
}

bool VRImgui::enabled() const
{
    return !mBlocked && mCurrentOpacity > 0.5f && mTargetOpacity > 0.5f;
}

} // namespace vr
} // namespace lava
