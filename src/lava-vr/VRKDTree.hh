#pragma once
#include <glm/vec3.hpp>
#include <string>
#include <vector>

// comment out for use on the phone
//#define VR_MESH_COMPILE_WRITING

typedef struct gzFile_s *gzFile;

namespace lava {
namespace vr {

struct VRMesh;
class VRKDTree {
  public:
    struct Cell {
        glm::vec3 bbmin;
        glm::vec3 bbmax;
        uint32_t first = 0;
        uint32_t count = 0;
        uint32_t firstChild = 0;
        bool isLeaf() const { return !firstChild; }
        // uint32_t  childRight = 0; always childLeft + 1
    };

#ifdef VR_MESH_COMPILE_WRITING
    /// setup kd-tree for the given mesh, caution: changes the order of
    /// triangles!
    void buildFor(VRMesh &mesh, size_t depth, size_t cell_size);
    void writeTo(gzFile &file) const;

    void writeToFile(const std::string &filename) const;
    void readFromFile(const std::string &filename);
#endif

    void readFrom(gzFile &file);

    std::vector<Cell> cells;
};

} // namespace vr
} // namespace lava
