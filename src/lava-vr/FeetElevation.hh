#include "VRMesh.hh"
#include <glm/fwd.hpp>
#include <lava-extras/openvr/fwd.hh>

namespace lava {
namespace vr {

class FeetElevation {
  public:
    FeetElevation(VRMesh const &mesh,
                  std::array<openvr::OpenVRController *, 2> feet);

    /// set the distance of the tracker from the floor when it's attached to a
    /// shoe
    void setHeightOffset(float val) { mHeightOffset = val; }
    /// set the absolute value of downwards acceleration when the user is
    /// falling
    void setFallingAcceleration(float val) { mFallingAccel = val; }
    /// set the absolute value of upwards acceleration when the user is close to
    /// a surface and being dragged up
    void setUpwardsAccel(float val) { mUpwardsAccel = val; }
    /// whenever the user would fall more than the given distance, no vertical
    /// movement will be triggered
    void setMaxFallHeight(float val) { mMaxFallHeight = val; }

    /// pass this the old scene matrix to get an updated one
    glm::mat4 apply(glm::mat4 oldMatrix);

    struct Tri {
        // glm::vec3 bbmin, bbmax;
        glm::vec3 a, b, c;

        Tri(glm::vec3 _a, glm::vec3 _b, glm::vec3 _c);
    };

  protected:
    std::array<lava::openvr::OpenVRController *, 2> mFeet;
    std::vector<Tri> mTriangles;
    float mVerticalSpeed = 0.0f;
    float mHeightOffset = 0.08f; // distance of tracker to floor
    float mFallingAccel = 1.f / 90.f;
    float mUpwardsAccel = 0.5f / 90.f;
    float mMaxFallHeight = 50.0f;
};

} // namespace vr
} // namespace lava
