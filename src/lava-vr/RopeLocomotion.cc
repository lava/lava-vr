#include "RopeLocomotion.hh"
#include <openvr.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/trigonometric.hpp>
#include <iostream>
#include <lava-extras/openvr/OpenVRControllerInput.hh>
#include <lava-extras/openvr/OpenVRHmd.hh>
#include <lava-extras/openvr/OpenVRTypes.hh>

namespace lava
{
namespace vr
{
RopeLocomotion::RopeLocomotion(const lava::openvr::SharedOpenVRHmd &output) : mOpenVR(output), mPressed({{false, false}}) {}

glm::mat4 RopeLocomotion::updateMatrix(glm::mat4 old)
{
    static auto triggerMask = ::vr::ButtonMaskFromId(::vr::EVRButtonId::k_EButton_SteamVR_Trigger);
    static auto touchpadMask = ::vr::ButtonMaskFromId(::vr::EVRButtonId::k_EButton_SteamVR_Touchpad);

    std::array<::vr::ETrackedControllerRole, 2> roles = {::vr::TrackedControllerRole_RightHand, ::vr::TrackedControllerRole_LeftHand};

    auto oldPoses = mLastPos;
    auto oldPressed = mPressed;
    auto touched = std::array<bool, 2>{{false, false}};

    auto system = ::vr::VRSystem();
    for (size_t i = 0; i < 2; i++)
    {
        auto hand = system->GetTrackedDeviceIndexForControllerRole(roles[i]);
        if (hand == ::vr::k_unTrackedDeviceIndexInvalid)
            continue;

        auto pose = mOpenVR->getPose(hand);
        if (!pose.bPoseIsValid)
            continue;

        ::vr::VRControllerState_t state;
        system->GetControllerState(hand, &state, sizeof(state));
        auto mat = glm::transpose(lava::vrToGlm(pose.mDeviceToAbsoluteTracking));
        mLastPos[i] = glm::vec3(mat[3]);
        mPressed[i] = (state.ulButtonPressed & triggerMask) && !mConfig.disableHand[i];
        touched[i] = state.ulButtonTouched & touchpadMask;
    }

    if (mPressed[0] && mPressed[1])
    {
        auto old_cog = 0.5f * (oldPoses[0] + oldPoses[1]);
        auto new_cog = 0.5f * (mLastPos[0] + mLastPos[1]);

        auto old_ray = 0.5f * (oldPoses[1] - oldPoses[0]);
        auto new_ray = 0.5f * (mLastPos[1] - mLastPos[0]);

        auto old_dir = glm::normalize(old_ray);
        auto new_dir = glm::normalize(new_ray);

        auto scale = (mConfig.allowZoom) ? glm::length(new_ray) / glm::length(old_ray) : 1.0f;

        auto axis = glm::vec3{0.f, 1.f, 0.f};
        auto angle = (mConfig.allowRotation) ? glm::orientedAngle(old_dir, new_dir, axis) : 0.0f;
        auto rot = glm::rotate(angle, axis);

        auto combined = glm::translate(new_cog) * rot * glm::scale(glm::vec3{scale, scale, scale}) * glm::translate(-old_cog);

        old = combined * old;
    }
    else if (mPressed[0])
    {
        old[3] += glm::vec4(mLastPos[0] - oldPoses[0], 0.f);
    }
    else if (mPressed[1])
    {
        old[3] += glm::vec4(mLastPos[1] - oldPoses[1], 0.f);
    }

    return old;
}

} // namespace vr
} // namespace lava
