#pragma once

#include <vector>
#include <string>

#include <glm/common.hpp>
#include <glm/mat4x4.hpp>

#include <lava/fwd.hh>

namespace lava {
namespace vr {

class BatchingRenderer {
  public:
    struct Vertex {
        glm::vec3 pos;
        glm::vec3 normal;
        glm::vec2 texCoord;
    };

    struct MeshHandle {
        uint32_t firstIndex;
        uint32_t count;
        uint32_t material;
    };

    BatchingRenderer(lava::Subpass const &forwardPass,
                     lava::SharedDescriptorSetLayout const &cameraLayout);

    MeshHandle add(std::string const &modelfile, std::string const &texture);
    void upload();

    void reset() { mDraws.clear(); }
    void enqueue(MeshHandle const &handle, glm::mat4 const &modelMatrix) {
        mDraws.emplace_back(handle, modelMatrix);
    }
    void draw(lava::InlineSubpass &sub,
              const lava::SharedDescriptorSet &cameraSet);

  protected:
    std::vector<Vertex> mVertices;
    std::vector<uint32_t> mIndices;
    std::vector<lava::SharedDescriptorSet> mMaterials;

    std::vector<std::pair<MeshHandle, glm::mat4>> mDraws;

    lava::SharedBuffer mVertexBuffer;
    lava::SharedBuffer mIndexBuffer;

    lava::SharedGraphicsPipeline mPipeline;
    lava::SharedDescriptorSetLayout mMaterialLayout;
    lava::SharedSampler mSampler;
};

} // namespace vr
} // namespace lava
