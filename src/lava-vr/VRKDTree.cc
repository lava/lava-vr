#include "VRKDTree.hh"
#include "VRMesh.hh"
#include <glm/glm.hpp>
#include <zlib.h>

#ifdef VR_MESH_COMPILE_WRITING
#include "ShortLambda.hh"
#include <iostream>
#include <range/v3/action/sort.hpp>
#include <range/v3/action/transform.hpp>
#include <range/v3/algorithm/nth_element.hpp>
#include <range/v3/view.hpp>
using namespace ranges;
#endif

namespace lava {
namespace vr {

using Tri = std::array<uint32_t, 3>;
using svec3 = std::array<float, 3>;

#ifdef VR_MESH_COMPILE_WRITING

/* Code needed for generating KDTrees,
 * not needed for display on the phone
 */

glm::vec3 toGLM(const svec3 &p) { return {p[0], p[1], p[2]}; }

constexpr float split_threshold = 1.5f;

void computeBounds(VRKDTree::Cell &cell, std::vector<Tri> &tris,
                   std::vector<VRMesh::Vertex> const &vertices) {
    cell.bbmin = glm::vec3(std::numeric_limits<float>::max());
    cell.bbmax = glm::vec3(-std::numeric_limits<float>::max());

    auto cellb = begin(tris) + cell.first;
    auto celle = cellb + cell.count;

    for (auto const &tri : make_iterator_range(cellb, celle)) {
        for (auto const &i : tri) {
            auto p = toGLM(vertices[i].position);
            cell.bbmin = glm::min(cell.bbmin, p);
            cell.bbmax = glm::max(cell.bbmax, p);
        }
    }
}

constexpr int search_range =
    512; // number of triangle tops around the median to check as better splits

struct KDSplit {
    int dim;
    float threshold;
};
static KDSplit getBestSplit(VRKDTree::Cell const &cell,
                            std::vector<Tri> const &tris,
                            std::vector<VRMesh::Vertex> const &vertices) {
    auto cellBegin = tris.begin() + cell.first;
    auto cellEnd = cellBegin + cell.count;
    std::cout << "Splitting cell with " << cell.count << " faces" << std::endl;

    KDSplit bestSplit = {0, 0.f};
    /*
        for (int i = 0; i < 3; ++i)
        {
            auto tops = tris |
                view::transform([&](Tri const& a) {
                    return std::max(vertices[a[0]].position[i],
                           std::max(vertices[a[1]].position[i],
                                    vertices[a[2]].position[i]));
                }) |
                to_vector;
            tops |= action::sort;

            auto startIndex = std::max(int(tops.size() / 2) - search_range, 0);
            auto endIndex = std::min(int(tops.size() / 2 + search_range),
       int(tops.size() - 1));

            for (auto idx = startIndex; idx < endIndex; ++idx)
            {
                auto threshold = tops[idx];

                auto inLeftBranch = [&](Tri const& tri) {
                    return std::all_of(begin(tri), end(tri),
       SL1(vertices[a].position[i] < threshold));
                };

                auto leftCount = size_t(std::count_if(cellBegin, cellEnd,
       inLeftBranch)); auto rightCount = cell.count - leftCount;

                auto cost = size_t(leftCount * leftCount + rightCount *
       rightCount);

                if (cost < bestSplit.cost)
                {
                    bestSplit = {i, threshold, leftCount, rightCount, cost};
                }
            }
        }
        std::cout << "Best split along " << bestSplit.dim
                  << " left: " << bestSplit.countLeft
                  << " right: " << bestSplit.countRight
                  << " cost: " << bestSplit.cost << std::endl;*/
    return bestSplit;
}

std::pair<VRKDTree::Cell, VRKDTree::Cell>
splitCell(VRKDTree::Cell const &cell, std::vector<Tri> &tris,
          std::vector<VRMesh::Vertex> const &vertices) {
    VRKDTree::Cell left;
    VRKDTree::Cell right;

    auto cellBegin = begin(tris) + cell.first;
    auto cellEnd = cellBegin + cell.count;

    auto split = getBestSplit(cell, tris, vertices);

    // std::cout << "Current cost " << cell.count * cell.count << " split cost "
    // << split.cost << std::endl; if (split.cost * 4 < cell.count * cell.count *
    // 3) {
    auto inLeftBranch = [&](Tri const &tri) {
        return std::all_of(
            begin(tri), end(tri),
            SL1(vertices[a].position[split.dim] < split.threshold));
    };

    auto border = std::partition(cellBegin, cellEnd, inLeftBranch);
    left.first = cell.first;
    left.count = uint32_t(border - cellBegin);
    right.first = uint32_t(border - begin(tris));
    right.count = uint32_t(cellEnd - border);
    std::cout << "Split along " << split.dim << std::endl;
    //}
    return {left, right};
}

std::pair<VRKDTree::Cell, VRKDTree::Cell>
splitCellAlong(VRKDTree::Cell const &cell, std::vector<Tri> &tris,
               std::vector<VRMesh::Vertex> const &vertices, int dim) {
    VRKDTree::Cell left;
    VRKDTree::Cell right;

    auto cellBegin = begin(tris) + cell.first;
    auto cellEnd = cellBegin + cell.count;

    auto tops = make_iterator_range(cellBegin, cellEnd) |
                view::transform([&](Tri const &a) {
                    return std::max(vertices[a[0]].position[dim],
                                    std::max(vertices[a[1]].position[dim],
                                             vertices[a[2]].position[dim]));
                }) |
                to_vector;
    tops |= action::sort;

    float threshold = tops[tops.size() / 2];

    auto inLeftBranch = [&](Tri const &tri) {
        return std::all_of(begin(tri), end(tri),
                           SL1(vertices[a].position[dim] <= threshold));
    };

    auto border = std::partition(cellBegin, cellEnd, inLeftBranch);
    left.first = cell.first;
    left.count = uint32_t(border - cellBegin);
    right.first = uint32_t(border - begin(tris));
    right.count = uint32_t(cellEnd - border);

    return {left, right};
}

std::pair<int, float> biggestDim(glm::vec3 const &v) {
    int max_idx = 0;
    float max_val = v[0];

    for (int i = 1; i < 3; ++i) {
        if (v[i] > max_val) {
            max_val = v[i];
            max_idx = i;
        }
    }

    return {max_idx, max_val};
}

void VRKDTree::buildFor(VR::VRMesh &mesh, size_t depth, size_t cell_size) {
    cells.clear();

    std::vector<Tri> tris;
    tris.reserve(mesh.indices.size() / 3);

    for (size_t i = 0; i < mesh.indices.size() - 2; i += 3) {
        Tri tri;
        tri[0] = mesh.indices[i + 0];
        tri[1] = mesh.indices[i + 1];
        tri[2] = mesh.indices[i + 2];
        tris.push_back(tri);
    }

    std::vector<size_t> levels;

    Cell root;
    root.count = uint32_t(tris.size());
    root.first = 0;
    computeBounds(root, tris, mesh.vertices);

    cells.push_back(root);
    levels.push_back(0);
    for (size_t i = 0; i < cells.size(); ++i) {
        cells.reserve(cells.size() + 2); // prevent reference invalidation
        Cell &current = cells[i];
        if (current.count <= cell_size) {
            std::cout << "Cell is small enough, aborting." << std::endl;
            continue;
        }

        if (levels[i] > depth) {
            std::cout << "Max depth reached for cell, aborting." << std::endl;
            continue;
        }

        // auto children = splitCell(current, tris, mesh.vertices);
        auto children =
            splitCellAlong(current, tris, mesh.vertices, levels[i] % 3);

        if (!children.first.count) {
            std::cout << "Can't split any further, aborting." << std::endl;
            continue;
        } else {
            std::cout << "Split cell: " << children.first.count << " / "
                      << children.second.count << std::endl;
        }

        computeBounds(children.first, tris, mesh.vertices);
        computeBounds(children.second, tris, mesh.vertices);

        current.firstChild = uint32_t(cells.size());
        cells.push_back(children.first);
        cells.push_back(children.second);
        levels.push_back(levels[i] + 1);
        levels.push_back(levels[i] + 1);
    }

    // update indices
    mesh.indices.clear();
    for (Tri const &t : tris) {
        for (auto i : t)
            mesh.indices.push_back(i);
    }

    // up to this point, indices are measured in triangles
    for (Cell &cell : cells) {
        cell.count *= 3;
        cell.first *= 3;
    }
    // now they are measured in vertices (for rendering)
}

void VRKDTree::writeTo(gzFile &file) const {
    uint32_t cellcount = cells.size();
    gzwrite(file, &cellcount, sizeof(uint32_t));
    gzwrite(file, cells.data(), sizeof(Cell) * cellcount);
    std::cout << "- BSP-cells: " << cells.size() << "\n";
}

void VRKDTree::writeToFile(const std::string &filename) const {
    auto file = gzopen(filename.c_str(), "w");
    writeTo(file);
    gzclose(file);
}

void VRKDTree::readFromFile(const std::string &filename) {
    auto file = gzopen(filename.c_str(), "r");
    readFrom(file);
    gzclose(file);
}

#endif

void VRKDTree::readFrom(gzFile &file) {
    uint32_t cellcount;
    gzread(file, &cellcount, sizeof(uint32_t));
    cells.resize(cellcount);
    gzread(file, cells.data(), sizeof(Cell) * cellcount);
}

} // namespace vr
} // namespace lava
