#include "FeetElevation.hh"
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <lava/common/log.hh>
#include <lava-extras/openvr/OpenVRControllerInput.hh>

namespace lava {
namespace vr {

FeetElevation::Tri::Tri(glm::vec3 _a, glm::vec3 _b, glm::vec3 _c)
    : a(_a), b(_b), c(_c) {
    // bbmin = glm::min(a, glm::min(b, c));
    // bbmax = glm::max(a, glm::min(b, c));
}

FeetElevation::Tri transform(FeetElevation::Tri const &orig,
                             glm::mat4 const &xf) {
    const auto nt = glm::mat3x4(xf);
    const auto t = xf[3];
    return FeetElevation::Tri(nt * orig.a + t, //
                              nt * orig.b + t, //
                              nt * orig.c + t);
}

FeetElevation::FeetElevation(
    const VRMesh &mesh,
    std::array<lava::openvr::OpenVRController *, 2> feet)
    : mFeet(feet) {
    const float thresh = cos(glm::radians(15.f));

    for (size_t i = 0; i < mesh.indices.size(); i += 3) {
        auto a = mesh.vertices[mesh.indices[i + 0]].position;
        auto b = mesh.vertices[mesh.indices[i + 1]].position;
        auto c = mesh.vertices[mesh.indices[i + 2]].position;

        auto n = glm::normalize(glm::cross(b - a, c - a));
        if (std::abs(n.z) < thresh)
            continue;

        mTriangles.emplace_back(a, b, c);
    }
    lava::debug() << "FootElevation: Identified " << mTriangles.size()
                  << " relevant triangles.";
}

glm::vec3 bary(glm::vec2 p, glm::vec2 a, glm::vec2 b, glm::vec2 c) {
    glm::vec2 v0 = b - a, v1 = c - a, v2 = p - a;
    float d00 = dot(v0, v0);
    float d01 = dot(v0, v1);
    float d11 = dot(v1, v1);
    float d20 = dot(v2, v0);
    float d21 = dot(v2, v1);
    float inv_denom = 1.f / (d00 * d11 - d01 * d01);

    float v = (d11 * d20 - d01 * d21) * inv_denom;
    float w = (d00 * d21 - d01 * d20) * inv_denom;
    float u = 1.0f - v - w;
    return {u, v, w};
}

glm::vec2 projy(glm::vec3 const &x) { return {x.x, x.z}; }

glm::mat4 FeetElevation::apply(glm::mat4 oldMatrix) {
    std::array<glm::vec3, 2> feet = {{{mFeet[0]->pose()[3]}, //
                                      {mFeet[1]->pose()[3]}}};

    float correction = -std::numeric_limits<float>::max();
    for (auto const &tri : mTriangles) {
        auto xt = transform(tri, oldMatrix);

        for (auto const &foot : feet) {
            glm::vec3 barys =
                bary(projy(foot), projy(xt.a), projy(xt.b), projy(xt.c));
            if (barys.x < 0 || barys.y < 0 || (barys.x + barys.y > 1))
                continue;

            float y = dot(barys, glm::vec3(xt.a.y, xt.b.y, xt.c.y));
            float local_corr = y - foot.y;
            if (local_corr < 0.03f)
                correction = std::max(y - foot.y, correction);
        }
    }
    correction += mHeightOffset;
    if (correction < -mMaxFallHeight)
        correction = 0.0f;

    if (std::signbit(correction) != std::signbit(mVerticalSpeed)) {
        mVerticalSpeed = std::copysign(0.0f, correction);
    } else {
        mVerticalSpeed += correction < 0.0 ? -mFallingAccel : mUpwardsAccel;
        // mVerticalSpeed += std::copysign(1.0f / 90.0f, correction);
    }
    correction = std::signbit(correction)
                     ? std::max(correction, mVerticalSpeed)
                     : std::min(correction, mVerticalSpeed);

    return glm::translate(glm::vec3(0, -correction, 0)) * oldMatrix;
}

} // namespace vr
} // namespace lava
