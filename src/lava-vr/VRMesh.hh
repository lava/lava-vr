#pragma once

#include "VRKDTree.hh"
#include <array>
#include <glm/vec2.hpp>
#include <vector>

namespace lava {
namespace vr {

struct VRMesh {
    struct Vertex {
        glm::vec3 position;
        glm::vec2 texCoord;
        uint32_t layer;
    };

    struct Texture {
        struct Meta {
            uint32_t width = 0;
            uint32_t height = 0;
            uint32_t depth = 0;
            uint32_t mipLayers = 0;
            uint32_t bytes_per_pixel = 1;
        } meta;

        std::vector<uint8_t> data;
    };
    using Color = std::array<uint8_t, 4>;

    struct Header {
        uint64_t version = 7;
        uint64_t num_verts = 0;
        uint64_t num_indcs = 0;
        uint8_t has_color_texture = false;
        uint8_t has_vertex_colors = false;
    };

    std::vector<uint32_t> indices;
    std::vector<Vertex> vertices;
    Texture colorTexture;
    std::vector<Color> colors;

    VRKDTree kdtree;

#ifdef VR_MESH_COMPILE_WRITING
    void writeToFile(const std::string &filename) const;
    void writeToObj(const std::string &filename) const;
    void buildKdTree(size_t depth, size_t cell_size);
#endif

    static VRMesh readFromFile(const std::string &filename);
    static VRMesh readFromObj(const std::string &objfile, const std::string& texture = "");

    struct OffsetHelper {
        size_t width, height, depth, bytes_per_pixel, mip_layers;

        ptrdiff_t getOffset(size_t layer, size_t mip) {
            ptrdiff_t result = 0;
            for (size_t i = 0; i < mip; i++) {
                result += bytesPerLayer(i) * depth;
            }

            size_t partial = bytesPerLayer(mip) * layer;
            result += partial;

            return result;
        }

        size_t totalSize() {
            size_t result = 0;
            for (size_t i = 0; i < mip_layers; i++) {
                result += bytesPerLayer(i) * depth;
            }
            return result;
        }

        size_t bytesPerLayer(size_t mip) {
            return (width >> mip) * (height >> mip) * bytes_per_pixel;
        }
    };
};
} // namespace vr
} // namespace lava
