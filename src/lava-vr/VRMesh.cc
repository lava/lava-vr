#include "VRMesh.hh"
#include <exception>
#include <iostream>
#include <assert.h>
#include <zlib.h>

#include <assimp/Importer.hpp>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <assimp/vector3.h>

#include <lodepng/lodepng.h>

namespace lava {
namespace vr {

#ifdef VR_MESH_COMPILE_WRITING

static void writeTexture(gzFile &to, VRMesh::Texture const &tex) {
    gzwrite(to, &tex.meta, sizeof(VRMesh::Texture::Meta));
    gzwrite(to, tex.data.data(), uint(tex.data.size()));
    std::cout << "- texture: " << tex.data.size() << " bytes\n";
}

void VRMesh::writeToFile(const std::string &filename) const {
    auto file = gzopen(filename.c_str(), "w");
    Header header;
    header.num_verts = vertices.size();
    header.num_indcs = indices.size();
    header.has_color_texture = !!colorTexture.meta.width;
    header.has_vertex_colors = !colors.empty();

    std::cout << "Writing VR-Mesh...\n";

    gzwrite(file, &header, sizeof(Header));
    gzwrite(file, vertices.data(), uint(sizeof(Vertex) * header.num_verts));
    std::cout << "- vertices: " << vertices.size() << "\n";
    gzwrite(file, indices.data(), uint(sizeof(uint32_t) * header.num_indcs));
    std::cout << "- indices: " << indices.size() << "\n";

    if (header.has_color_texture) {
        writeTexture(file, colorTexture);
    }

    if (header.has_vertex_colors) {
        uint64_t colorcount = colors.size();
        gzwrite(file, &colorcount, sizeof(uint64_t));
        gzwrite(file, colors.data(), sizeof(Color) * colors.size());
    }

    assert(kdtree.cells.size());
    kdtree.writeTo(file);
    std::cout << std::endl;

    gzclose(file);
}

void VRMesh::writeToObj(const std::string &filename) const {
    auto file = fopen(filename.c_str(), "w");
    for (auto const &v : vertices) {
        auto const &p = v.position;
        fprintf(file, "v %f %f %f\n", p[0], p[1], p[2]);
    }

    for (size_t i = 0; i < indices.size() - 2; i += 3) {
        fprintf(file, "f %i %i %i\n", indices[i] + 1, indices[i + 1] + 1,
                indices[i + 2] + 1);
    }

    fclose(file);
}

void VRMesh::buildKdTree(size_t depth, size_t cell_size) {
    kdtree.buildFor(*this, depth, cell_size);
}

#endif

static VRMesh::Texture readTexture(gzFile &from) {
    VRMesh::Texture tex;
    gzread(from, &tex.meta, sizeof(VRMesh::Texture::Meta));

    VRMesh::OffsetHelper helper;
    helper.width = tex.meta.width;
    helper.height = tex.meta.height;
    helper.bytes_per_pixel = tex.meta.bytes_per_pixel;
    helper.mip_layers = tex.meta.mipLayers;
    helper.depth = tex.meta.depth;

    size_t num_bytes = helper.totalSize();
    tex.data.resize(num_bytes);

    gzread(from, tex.data.data(), uint32_t(num_bytes));

    return tex;
}

VRMesh VRMesh::readFromFile(const std::string &filename) {
    VRMesh result;

    auto file = gzopen(filename.c_str(), "r");

    Header header;
    gzread(file, &header, sizeof(Header));
    if (header.version != Header().version)
        throw std::runtime_error("File and Loader version don't match up!");

    result.vertices.resize(header.num_verts);
    gzread(file, result.vertices.data(),
        uint32_t(sizeof(Vertex) * header.num_verts));
    result.indices.resize(header.num_indcs);
    gzread(file, result.indices.data(),
        uint32_t(sizeof(uint32_t) * header.num_indcs));

    if (header.has_color_texture) {
        result.colorTexture = readTexture(file);
    }

    if (header.has_vertex_colors) {
        uint64_t colorcount;
        gzread(file, &colorcount, sizeof(uint64_t));
        result.colors.resize(colorcount);
        gzread(file, result.colors.data(), sizeof(Color) * colorcount);
    }

    result.kdtree.readFrom(file);

    gzclose(file);

    return result;
}

VRMesh VRMesh::readFromObj(const std::string &objfile,
                           const std::string &texture) {
    VRMesh result;

    Assimp::Importer importer;

    const aiScene *importScene =
        importer.ReadFile(objfile, AI_SCENE_FLAGS_NON_VERBOSE_FORMAT |
                                       aiProcess_JoinIdenticalVertices);

    if (importScene == NULL) {
        std::cout << "VRMesh::readFromObj(...): no scene loaded." << std::endl
                  << "Perhaps invalid filepath given? Filepath is:" << std::endl
                  << objfile << std::endl;
        assert(0);
    }

    if (importScene->HasMeshes()) {
        aiMesh *theMesh = importScene->mMeshes[0];
        auto uv = theMesh->mTextureCoords[0];

        for (uint32_t i = 0; i < theMesh->mNumVertices; ++i) {
            Vertex v;
            v.position.x = theMesh->mVertices[i].x;
            v.position.y = theMesh->mVertices[i].y;
            v.position.z = theMesh->mVertices[i].z;
            if (uv) {
                v.texCoord.x = uv[i].x;
                v.texCoord.y = 1.0 - uv[i].y;
            }
            v.layer = 0.0f;

            result.vertices.push_back(v);
        }
        for (uint32_t i_f = 0; i_f < theMesh->mNumFaces; ++i_f) {
            assert(theMesh->mFaces[i_f].mNumIndices == 3 &&
                   "Only triangles for now please");
            for (uint32_t i_i = 0; i_i < 3; ++i_i) {
                uint32_t currentVertexIndex =
                    (uint32_t)theMesh->mFaces[i_f].mIndices[i_i];
                result.indices.push_back(currentVertexIndex);
            }
        }

        if (!texture.empty()) {
            result.colorTexture.meta.depth = 1;
            result.colorTexture.meta.mipLayers = 1;
            result.colorTexture.meta.bytes_per_pixel = 4;
            lodepng::decode(result.colorTexture.data,
                            result.colorTexture.meta.width,
                            result.colorTexture.meta.height, texture, LCT_RGBA);
        } else {
            result.colorTexture.meta.width = 1;
            result.colorTexture.meta.height = 1;
            result.colorTexture.meta.depth = 1;
            result.colorTexture.meta.mipLayers = 1;
            result.colorTexture.meta.bytes_per_pixel = 4;
            result.colorTexture.data = {0xff, 0xff, 0xff, 0xff};
        }
    } else {
        std::cout
            << "Loading VRMesh from .obj-file failed (no mesh could be loaded)."
            << std::endl;
        assert(0);
    }

    return result;
}

} // namespace vr
} // namespace lava
