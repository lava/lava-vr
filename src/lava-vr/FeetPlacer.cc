#include "FeetPlacer.hh"
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <lava-extras/openvr/OpenVRControllerInput.hh>

namespace lava {
namespace vr {

FeetPlacer::FeetPlacer(MeshHandle foot, Tracker const &left,
                       Tracker const &right)
    : mFoot(foot), mLeft(left), mRight(right) {}

void FeetPlacer::place(BatchingRenderer &renderer) {
    glm::mat4 trackerToShoe = {1, 0, 0,  0, //
                               0, 0, -1, 0, //
                               0, 1, 0,  0, //
                               0, 0, 0,  1};
    // trackerToShoe = glm::mat4(1.0);

    {
        auto leftXf = mLeft.pose() * trackerToShoe;
        leftXf = leftXf * scale(glm::vec3{-1.f, 1.f, 1.f});

        renderer.enqueue(mFoot, leftXf);
    }
    {
        auto rightXf = mRight.pose() * trackerToShoe;
        renderer.enqueue(mFoot, rightXf);
    }
}

} // namespace vr
} // namespace lava
