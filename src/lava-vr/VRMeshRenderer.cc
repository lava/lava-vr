#include "VRMeshRenderer.hh"
#include <glm/glm.hpp>
#include <lava-extras/pack/pack.hh>
#include <lava/common/log.hh>
#include <lava/createinfos/Buffers.hh>
#include <lava/createinfos/DescriptorSetLayoutCreateInfo.hh>
#include <lava/createinfos/GraphicsPipelineCreateInfo.hh>
#include <lava/createinfos/Images.hh>
#include <lava/createinfos/Sampler.hh>
#include <lava/objects/Buffer.hh>
#include <lava/objects/DescriptorSet.hh>
#include <lava/objects/DescriptorSetLayout.hh>
#include <lava/objects/Device.hh>
#include <lava/objects/GraphicsPipeline.hh>
#include <lava/objects/Image.hh>
#include <lava/objects/RenderPass.hh>
#include <lava/raii/ActiveRenderPass.hh>

#include "VRMesh.hh"

namespace lava {
namespace vr {

namespace {
using Vertex = VRMesh::Vertex;

struct OffsetHelper {
    size_t width, height, depth, bytes_per_pixel, mip_layers;

    ptrdiff_t getOffset(size_t layer, size_t mip) {
        ptrdiff_t result = 0;
        for (size_t i = 0; i < mip; i++) {
            size_t block =
                (width >> i) * (height >> i) * depth * bytes_per_pixel;
            result += block;
        }

        size_t partial =
            (width >> mip) * (height >> mip) * layer * bytes_per_pixel;
        result += partial;

        return result;
    }

    size_t bytesPerLayer(size_t mip) {
        return (width >> mip) * (height >> mip) * bytes_per_pixel;
    }
};
} // namespace

VRMeshRenderer::VRMeshRenderer(
    const lava::Subpass &forwardPass,
    const lava::SharedDescriptorSetLayout &cameraLayout) {
    auto device = forwardPass.pass->device();

    mTextureLayout = lava::DescriptorSetLayoutCreateInfo() //
                         .addCombinedImageSampler()
                         .create(device);
    mLayout =
        device->createPipelineLayout<glm::mat4>({cameraLayout, mTextureLayout});

    auto ci = lava::GraphicsPipelineCreateInfo::defaults();
    ci.setLayout(mLayout);

    ci.depthStencilState.setDepthTestEnable(true).setDepthWriteEnable(true);
    ci.rasterizationState.setFrontFace(vk::FrontFace::eClockwise);

    ci.addStage(lava::pack::shader(device, "lava-vr/vrmesh.vert"));
    ci.addStage(lava::pack::shader(device, "lava-vr/vrmesh.frag"));

    ci.vertexInputState.addAttribute(&Vertex::position, 0);
    ci.vertexInputState.addAttribute(&Vertex::texCoord, 1);
    ci.vertexInputState.addAttribute(&Vertex::layer, 2);

    mPipeline = forwardPass.createPipeline(ci);

    mSampler = device->createSampler({});
}

void VRMeshRenderer::loadFile(const std::string &filename) {
    auto mesh = VRMesh::readFromFile(filename);
    upload(mesh);
}

void VRMeshRenderer::upload(const VRMesh &mesh) {
    auto const &device = mPipeline->device();

    lava::debug() << "Loaded mesh with " << mesh.indices.size() / 3u
                  << " triangles:";

    mVertexBuffer = device->createBuffer(lava::arrayBuffer());
    mVertexBuffer->setDataVRAM(mesh.vertices);

    mIndexBuffer = device->createBuffer(lava::indexBuffer());
    mIndexBuffer->setDataVRAM(mesh.indices);

    {
        auto meta = mesh.colorTexture.meta;

        mTexture = lava::texture2DArray(meta.width, meta.height, meta.depth,
                                        meta.bytes_per_pixel == 1
                                            ? vk::Format::eBc7SrgbBlock
                                            : vk::Format::eR8G8B8A8Srgb)
                       .setMipLevels(meta.mipLayers)
                       .create(device);

        mTexture->realizeVRAM();
        mTexture->changeLayout(vk::ImageLayout::eTransferDstOptimal);

        auto staging = device->createBuffer(lava::stagingBuffer());
        staging->setDataRAM(mesh.colorTexture.data);

        std::vector<vk::BufferImageCopy> copies;

        OffsetHelper helper;
        helper.width = meta.width;
        helper.height = meta.height;
        helper.depth = meta.depth;
        helper.bytes_per_pixel = meta.bytes_per_pixel;
        helper.mip_layers = meta.mipLayers;

        for (uint32_t mip = 0; mip < meta.mipLayers; mip++) {
            for (uint32_t layer = 0; layer < meta.depth; layer++) {
                vk::ImageSubresourceLayers sub;
                sub.setAspectMask(vk::ImageAspectFlagBits::eColor)
                    .setMipLevel(mip)
                    .setBaseArrayLayer(layer)
                    .setLayerCount(1);

                vk::Extent3D extent;
                extent.setWidth(meta.width >> mip)
                    .setHeight(meta.height >> mip)
                    .setDepth(1);

                vk::BufferImageCopy copy;
                copy.setImageSubresource(sub)
                    .setImageExtent(extent)
                    .setBufferOffset(helper.getOffset(layer, mip));

                copies.push_back(copy);
            }
        }

        auto cmd = device->graphicsQueue().beginCommandBuffer();
        cmd->copyBufferToImage(staging->handle(), mTexture->handle(),
                               vk::ImageLayout::eTransferDstOptimal, copies);
        mTexture->changeLayout(vk::ImageLayout::eTransferDstOptimal,
                               vk::ImageLayout::eShaderReadOnlyOptimal, cmd);
    }

    mTextureSet = mTextureLayout->createDescriptorSet();
    mTextureSet->write().combinedImageSampler(mSampler, mTexture->createView());
}

void VRMeshRenderer::draw(lava::InlineSubpass &sub,
                          const lava::SharedDescriptorSet &cameraSet) {
    sub.bindPipeline(mPipeline);

    sub.bindDescriptorSets({cameraSet, mTextureSet});
    sub.bindVertexBuffers({mVertexBuffer});
    sub.bindIndexBuffer(mIndexBuffer);

    sub.pushConstantBlock(mModelMatrix);

    sub.drawIndexed(mIndexBuffer->size() / sizeof(uint32_t));
}

} // namespace vr
} // namespace lava
