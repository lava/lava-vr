#pragma once

namespace lava {
namespace vr {


#define LAVA_FORWARD_DECLARE_CLASS(T) \
    class T; \
    using Shared ## T = std::shared_ptr<T>; \
    using Weak ## T = std::weak_ptr<T>; \
    using Unique ## T = std::unique_ptr<T>

LAVA_FORWARD_DECLARE_CLASS(BatchingRenderer);
LAVA_FORWARD_DECLARE_CLASS(FeetElevation);
LAVA_FORWARD_DECLARE_CLASS(FeetPlacer);
LAVA_FORWARD_DECLARE_CLASS(RopeLocomotion);
LAVA_FORWARD_DECLARE_CLASS(VRMesh);
LAVA_FORWARD_DECLARE_CLASS(VRMeshRenderer);
LAVA_FORWARD_DECLARE_CLASS(VRImgui);

}
} // namespace lava
