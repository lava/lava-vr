#pragma once

#include <glm/mat4x4.hpp>
#include <lava-extras/imgui/fwd.hh>
#include <lava-extras/openvr/fwd.hh>
#include <lava/fwd.hh>

namespace lava {
namespace vr {

struct VRImGuiFrame {
    VRImGuiFrame() : active(true){};
    VRImGuiFrame(VRImGuiFrame const &) = delete;
    VRImGuiFrame(VRImGuiFrame &&o) { o.active = false; }
    ~VRImGuiFrame();

    bool active;
};

class VRImgui {
  public:
    VRImgui(lava::Subpass const &forwardPass,
            lava::SharedDescriptorSetLayout const &cameraLayout,
            float width = 0.4f, float height = 0.25f);

    VRImGuiFrame frame();

    /// call one of the update() variants after you finished recording the
    /// frame, but before draw().
    /// This particular variant uses a single point as
    /// "finger", i.e. getting close enough to the screen is registered as a
    /// "click"
    void update(glm::mat4 const &screenPose, glm::vec3 const &finger,
                lava::RecordingCommandBuffer &cmd);

    // TODO: Implement the raytracing pendant to the update above.
    //void update(glm::mat4 const &screenPose, glm::vec3 const &origin,
    //            glm::vec3 const &direction, bool pressed,
    //            lava::RecordingCommandBuffer &cmd);

    void draw(lava::InlineSubpass &cmd, const SharedDescriptorSet &cameraSet);

    void fade(bool in);
    void toggle();

    /// returns true if the ImGui will render anything (due to not being faded out),
    /// you can skip recording widgets if this returns false
    bool visible() const;

    /// returns true if the ImGui is accepting input (due to fading state, or being blocked)
    bool enabled() const;

    void setBlocked(bool val) { mBlocked = val; };

  protected:
    SharedGraphicsPipeline mPipeline;
    SharedDescriptorSet mDescriptor;
    float mWidth, mHeight;
    float mCurrentOpacity = 1.0f, mTargetOpacity = 1.0f;
    glm::mat4 mPose;
    glm::vec3 mVMouse;
    bool mBlocked = false;

    imgui::UniqueImGui mImgui;
};

} // namespace vr
} // namespace lava
